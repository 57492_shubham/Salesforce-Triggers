/**
 * @description       : 
 * @author            : Shubham Jaurat
 * @group             : 
 * @last modified on  : 01-18-2025
 * @last modified by  : Shubham Jaurat
**/
trigger CaseTriggers on case (after insert) {
   
    if(trigger.isBefore && trigger.isInsert){
        CaseTriggersController.updateConactfield(trigger.new);
    }
}