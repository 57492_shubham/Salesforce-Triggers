/**
 * @description       : 
 * @author            : Shubham Jaurat
 * @group             : 
 * @last modified on  : 01-23-2025
 * @last modified by  : Shubham Jaurat
**/
trigger TaskTriggers on Task (after update) {
   
    if(trigger.isafter && trigger.isUpdate){
        TaskTriggerController.UpdateAccountRatingWhenStatusIsComplete(trigger.newMap);
    }    
}