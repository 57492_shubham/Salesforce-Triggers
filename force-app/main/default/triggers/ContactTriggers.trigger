/**
 * @description       : 
 * @author            : Shubham Jaurat
 * @group             : 
 * @last modified on  : 01-03-2025
 * @last modified by  : Shubham Jaurat
**/

trigger ContactTriggers on Contact (after update,after insert) {


    if(trigger.isAfter && trigger.isUpdate)
    {
      
      //ContactTriggerController.updateAccountPhonAndRelatedContactsPhone(Trigger.NewMap,Trigger.OldMap);  
    }

    if(trigger.isAfter && trigger.isInsert)
    {
      //ContactTriggerController.createCloneContact(Trigger.New);  
    }

   if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate))
    {
      ContactTriggerController.UpdateAccountWhenContactCheckFieldIsChecked(Trigger.New);  
    }


}