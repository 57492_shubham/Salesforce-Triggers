/**
 * @description       : 
 * @author            : Shubham Jaurat
 * @group             : 
 * @last modified on  : 10-05-2024
 * @last modified by  : Shubham Jaurat
**/
public with sharing class FinancialAccTransactionTriggerController {

// 1) where money is withdrawn from an ATM or transferred via UPI and the financial account balances are updated accordingly
// use Financial  Account Transaction object 
    
    public static void UpdateFinancialAccountBalance(List<FinServ__FinancialAccountTransaction__c> FinancialAccountTransactionList )
    {
        List<FinServ__FinancialAccount__c> FinancialAccountList = new List<FinServ__FinancialAccount__c>();
        Map<ID, trasantion> FinancialAccountToUpdate = new Map<ID, trasantion>();

        for (type FinancialAccountTransaction : FinancialAccountTransactionList) 
        {
            trasantion trasantionObj = new trasantion(FinancialAccountTransaction.FinServ__TransactionType__c , FinancialAccountTransaction.FinServ__Amount__c);
            FinancialAccountToUpdate.put(FinancialAccountTransaction.FinServ__FinancialAccount__c, trasantionObj);
        }
        
        FinancialAccountList = [SELECT Id, FinServ__Balance__c FROM FinServ__FinancialAccount__c Where Id IN : FinancialAccountToUpdate.keySet()];
        
        for( type FinancialAccount : FinancialAccountList )
         {
            
            if(FinancialAccountToUpdate.containsKey(FinancialAccount.Id))
             {
                trasantion tranObj = FinancialAccountToUpdate.get(FinancialAccount.Id);
                String type = tranObj.type;
                Integer Amount = tranObj.Amount;

                if(type == 'Debit')
                {
                    FinancialAccount.FinServ__Balance__c -= Amount;
                    
                }else if(type == 'Credit')
                {
                    FinancialAccount.FinServ__Balance__c += Amount;
                }

            }

        }

        Database.SaveResult[] updateResults = Database.update(FinancialAccountList);
    
    }

    public class trasantion {
        public Integer Amount;
        public String Type;
        
    
        // Constructor
        public trasantion(String Type, Integer Amount) {
            this.Amount = Amount;
            this.Type = Type;
        }
    }


}