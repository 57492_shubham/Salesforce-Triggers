/**
 * @description       : 
 * @author            : Shubham Jaurat
 * @group             : 
 * @last modified on  : 08-18-2024
 * @last modified by  : Shubham Jaurat
**/
public with sharing class OpportunityTriggerController {
    //Update ParaentAccout With Opprtunity having Highest mount
    public static void UpdateParaentAccoutWithOpprtunity(Map<Id, Opportunity> newOpportunityMap ){
         
        Set<Id> AccountIds = new Set<Id>();
        Map<Id, String> paraentAccountIdWithOppName = new Map<Id, String>();
        List<Account> AccountListWithHighestAmountOpprtunity = new List<ElementType>();
        List<Account> ToUpdate = new List<Account>();
        if(!newOpportunityMap.isEmpty()){
            for ( Opportunity opp : newOpportunityMap.values()) {
                AccountIds.add(opp.AccountId);
            }
        }
        
        if(!AccountIds.isEmpty()){
            AccountListWithHighestAmountOpprtunity = [SELECT Id, Name, 
                                                            (SELECT Id, Name, Amount 
                                                            FROM Opportunities 
                                                            ORDER BY Amount DESC 
                                                            LIMIT 1)
                                                    FROM Account
                                                    WHERE Id IN AccountIds] ;
        }
    
        if(!AccountListWithHighestAmountOpprtunity.isEmpty()){
            for (Account acc : AccountListWithHighestAmountOpprtunity) {
                paraentAccountIdWithOppName.put(acc.ParentId, acc.Opportunities.Name );
            }
        }
        
        ToUpdate = [SELECT Id, Name, max_opp__c FROM Account WHERE Id IN paraentAccountIdWithOppName.keySet() ]

        for (Account acc : ToUpdate) {
            acc.max_opp__c = paraentAccountIdWithOppName.get(acc.Id);
        }
        update ToUpdate;
        
    }

 
}