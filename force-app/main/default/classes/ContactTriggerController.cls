/**
 * @description       : 
 * @author            : Shubham Jaurat
 * @group             : 
 * @last modified on  : 01-03-2025
 * @last modified by  : Shubham Jaurat
**/
public with sharing class ContactTriggerController {
   // write a trigger that updates the Parent Account Phone and All Related Contact's Phone  Whenever the Contact Phone Fields is Updated.
   public static Boolean isTriggerRunning = false;


   public static void updateAccountPhonAndRelatedContactsPhone(Map<Id,Contact> newContactMap, Map<Id,Contact> oldContactMap)
   {    

    List<Account> ParaentAccounts = new List<Account>();

    Set<Id> accountIds = new Set<Id>();

    List<Contact> relatedContacts = new List<Contact>();

    List<Contact> UpdatedRelatedContacts = new List<Contact>();

    Map<Id, Contact> ContactsWithAccountIdAsKey= new Map<Id, Contact>();

        for (Id con : newContactMap.keySet())
        {
            Contact newContact = newContactMap.get(con);
            Contact OldContact = oldContactMap.get(con);

                if(newContact.Phone != OldContact.Phone )
                {
                    accountIds.add(newContact.AccountId);
                    ContactsWithAccountIdAsKey.put(newContact.AccountId, newContact);

                }     
        }
        
        ParaentAccounts = [SELECT Id, Phone 
                           FROM Account 
                           WHERE ID IN : accountIds ];

        if(ParaentAccounts.size() > 0){
            for (Account acc : ParaentAccounts ) {
                if(ContactsWithAccountIdAsKey.containsKey(acc.Id))
                {
                    acc.Phone = ContactsWithAccountIdAsKey.get(acc.Id).Phone;
                }
            }
        }

        Update ParaentAccounts;

        if(ParaentAccounts.size() > 0)
        {   
            relatedContacts = [SELECT Id, Phone, AccountId
                               FROM Contact
                               WHERE AccountId IN : accountIds];
        }
        
        for (Contact con : relatedContacts) {
            if(ContactsWithAccountIdAsKey.containsKey(con.AccountId))
                {   
                    if(con.Phone != ContactsWithAccountIdAsKey.get(con.AccountId).Phone )
                    {
                        con.Phone = ContactsWithAccountIdAsKey.get(con.AccountId).Phone;
                    }
                }

        }
        
        update relatedContacts;
        
   }


//    Write trigger On contact 
// when Contact is created Create clone of that related contact 

public static Void createCloneContact(List<contact> newContact  ){
    if (isTriggerRunning) {
        return;
    }
    
    isTriggerRunning = true;

   try {
    List<Contact> contactsToClone = new List<Contact>(); 
    for (contact con : newContact) {
        contact clonedCon = con.clone(false, false, false, false);
        contactsToClone.add(clonedCon);
    }
    if(!contactsToClone.isEmpty()){
      Insert contactsToClone;
    }
    
   } catch (Exception e) {
    System.debug('Exception'+e);
   } finally {
       isTriggerRunning = false;
   }
    

}

// UpdateAccountWhenContactCheckFieldIsChecked


// and when User uncheck contact it should check related contact is checked or not Is all related contacts are unchecked then unchecked account

public static void UpdateAccountWhenContactCheckFieldIsChecked(List<Contact> newContacts ){
    List<Account> accountList = new List<Account>();
    List<ID> accountIds = new List<ID>();
    
    if(!newContacts.isEmpty()){
        for (Contact Con : newContacts) {
            if(con.check__c == true){
              accountIds.add(con.AccountId);
            }
        }
    }
      
    accountList = [SELECT Id,check__c FROM Account WHERE Id IN : accountIds];
    
    if(!accountList.isEmpty()){

        for( Account acc : accountList ) {
            acc.check__c = true;
        }

        update accountList;
    }
    
}

}