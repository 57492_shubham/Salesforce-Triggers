/**
 * @description       : 
 * @author            : Shubham Jaurat
 * @group             : 
 * @last modified on  : 01-23-2025
 * @last modified by  : Shubham Jaurat
**/

public with sharing class TaskTriggerController {
    
    //1) write a trigger on task When status is 'Complet'
    // and task related to Account then account rating Field should update according to the task rating field 
    //    SELECT Id, Status, WhoId, WhatId, Subject, Priority, AccountId FROM Task


   public static void UpdateAccountRatingWhenStatusIsComplete(Map<Id, Task> newTaskMap){
    List<account> accountList = new List<account>();
    List<account> accountListToUpdate = new List<account>();
    List<Id> AccountIdList = new List<Id>();

    if(!newTaskMap.isEmpty()){
       for( Task taskRecord : newTaskMap.values()) {
           if (taskRecord.AccountId != null) {
             AccountIdList.add(taskRecord.AccountId);
           }
       }
    }

    accountList = [SELECT Id, Name, Rating FROM Account Where Id IN : AccountIdList ];

    Map<Id, Account> accountMap = new Map<Id, Account>(accountList);

    if(!newTaskMap.isEmpty()){
        for( Task taskRecord : newTaskMap.values()) {
            if (taskRecord.AccountId != null && taskRecord.Status == 'Completed' ) {
                Account Acc = accountMap.get(taskRecord.AccountId);
                if(taskRecord.Priority != null){
                    if(taskRecord.Priority == 'High'){
                        Acc.Rating = 'Hot';
                    }else if( taskRecord.Priority == 'Normal' ){ 
                        Acc.Rating = 'Warm';
                    }else if( taskRecord.Priority == 'Low' ){ 
                        Acc.Rating = 'Cold';
                    }
                    accountListToUpdate.add(Acc);
                } 
            }
        }
    }
    
    If(!accountListToUpdate.isEmpty()){
        Update accountListToUpdate;
    }

   } 
}