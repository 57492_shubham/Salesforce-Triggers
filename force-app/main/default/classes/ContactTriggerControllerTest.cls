/**
 * @description       : 
 * @author            : Shubham Jaurat
 * @group             : 
 * @last modified on  : 09-02-2024
 * @last modified by  : Shubham Jaurat
**/

/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an org are executed whenever Apex code is deployed
 * to a production org to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production org. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the org size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ContactTriggerControllerTest {

    @isTest
    static void testCloneContactOnInsert() {
        // Create a Contact record
        Contact originalContact = new Contact(
            FirstName = 'Test',
            LastName = 'Contact'
        );
        insert originalContact;
        
        // Retrieve the cloned Contact
        List<Contact> clonedContacts = [SELECT Id, FirstName, LastName FROM Contact WHERE LastName LIKE '% - Clone'];
        
        // Assert that the cloned contact exists
        System.assertEquals(1, clonedContacts.size(), 'There should be one cloned contact');
        System.assertEquals(originalContact.FirstName, clonedContacts[0].FirstName, 'First name should be the same as original');
        System.assertEquals(originalContact.LastName + ' - Clone', clonedContacts[0].LastName, 'Last name should end with " - Clone"');
        
        // Check that no additional clones were created
        List<Contact> allContacts = [SELECT Id FROM Contact];
        System.assertEquals(2, allContacts.size(), 'Only original and one clone should exist');
    }
}