/**
 * @description       : 
 * @author            : shubham jaurat
 * @group             : 
 * @last modified on  : 08-18-2024
 * @last modified by  : Shubham Jaurat
**/
public with sharing class AccountTriggerController {
    // 1. Write a trigger on the Account when the Account is updated check all opportunities related to the account. 
    // Update all Opportunities Stage to close lost if an opportunity created date is greater than 30 days from 
    // today and stage not equal to close won.
  
    public static void accountUpdated(List<Account> accountList ){
        try {
            Set<ID> acountIDs = new Set<ID>();

            for (Account acc : accountList) {
                acountIDs.add(acc.id);
            }

            Datetime dateBefore30Days = datetime.now().addDays(-30);

            List<Opportunity> opportunityList = [Select Id, AccountId, StageName, CreatedDate, CloseDate from Opportunity where AccountId in :acountIDs WITH SECURITY_ENFORCED];

            if(!opportunityList.isEmpty()){
                for (Opportunity opp : opportunityList) {
                    if(opp.CreatedDate > dateBefore30Days && opp.StageName !='Closed Won'){
                        opp.StageName = 'Closed Lost';
                    }
                }
            }
            if(!opportunityList.isEmpty()){
                
                update opportunityList;
            }
        
        } catch (Exception e) {
        System.debug(e);
        }
    }
    /*
    2. While inserting account if a User left The phone Field Empty Then n error should come stating
     "You cannot left the phone field empty".
      */ 

   public static void phonFieldCanNotEmpty(List<Account> accountList){
        if(!accountList.isEmpty()){
            for (Account acc : accountList) {
                if(acc.Phone == null){
                    // acc.addError('You cannot left the phone field empty');  error bottom of edit view form
                    acc.Phone.addError('You cannot left the phone field empty'); //error bottom of edit view form 
                }
            }
        }
   } 

   /*
     While inserting account if a User left The Billing Address empty Field Empty Then 
     Automatic Populate Billing address field same as like shipping address.

     also add error if Shipping address is not fill . 
     error  -------> "please enter [fieldName]"
     field api names
     Billing address  --> BillingStreet, BillingCity, BillingPostalCode, BillingState, BillingCountry, BillingLatitude, BillingLongitude, BillingGeocodeAccuracy, BillingAddress,
     Shipping address --> ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry, ShippingLatitude, ShippingLongitude, ShippingGeocodeAccuracy, ShippingAddress
   */

   public static void automaticPopulateBillingAddress(List<Account> accountList){
        if(!accountList.isEmpty()){
            for (Account acc : accountList) {
                // acc.ShippingStreet == null ? acc.ShippingStreet.addError('Please enter Shipping Street ') : acc.BillingStreet = acc.ShippingStreet ;
                // acc.ShippingCity == null ? acc.ShippingCity.addError('Please enter Shipping City ') : acc.BillingCity = acc.ShippingCity ;
                // acc.ShippingState == null ? acc.ShippingState.addError('Please enter Shipping State') : acc.BillingState = acc.ShippingState;
                // acc.ShippingPostalCode == null ? acc.ShippingPostalCode.addError('Please enter Shipping City ') : acc.BillingPostalCode = acc.ShippingPostalCode ;

                if (acc.ShippingStreet == null) {
                    acc.ShippingStreet.addError('Please enter Shipping Street');
                }  else if(acc.BillingStreet != null){
                    acc.BillingStreet = acc.ShippingStreet;
                }
                
                if (acc.ShippingCity == null) {
                    acc.ShippingCity.addError('Please enter Shipping City');
                } else if(acc.BillingCity != null) {
                    acc.BillingCity = acc.ShippingCity;
                }
                
                if (acc.ShippingState == null) {
                    acc.ShippingState.addError('Please enter Shipping State');
                } else if(acc.BillingState != null) {
                    acc.BillingState = acc.ShippingState;
                }
                
                if (acc.ShippingPostalCode == null) {
                    acc.ShippingPostalCode.addError('Please enter Shipping Postal Code');
                } else if(acc.BillingPostalCode != null){
                    acc.BillingPostalCode = acc.ShippingPostalCode;
                }
            }
        }
   }


   /*
   3) Whenever Account field is Updated then all related contact's Phone phonr field should automatically updated with  pararaent account phone.
   */
    public static void updateContactPhoneRelatedToAccount(List<Account> accountListNew , Map<Id, Account> accountsMapOld){
        Map<id, Account> updatedaccounts = new Map<id, Account>();
        if(!accountListNew.isEmpty()){
            for (Account acc : accountListNew) {
                if(accountsMapOld.get(acc.id).Phone != acc.Phone ){
                    updatedaccounts.put(acc.id , acc);
                }
            }
        }
        List<contact> relatedContacts =[SELECT Id,AccountId,Phone FROM contact WHERE accountId IN: updatedaccounts.keySet() WITH SECURITY_ENFORCED] ;
        List<contact> listToUpdateContact = new List<contact>();
        if(!relatedContacts.isEmpty()){
            for (Contact con : relatedContacts) {
                con.Phone =  updatedaccounts.get(con.AccountId).phone;
                listToUpdateContact.push(con);
            }
        }
        if(!listToUpdateContact.isEmpty()){
            update listToUpdateContact;
            // List<Database.SaveResult> updateResults = Database.update(recordsToUpdate, allOrNone);
        } 
    
  }
  
//   Trigger on Account 
// Context - After Create & After Update
// Traverse over created or updated accounts list from Trigger.new
// And split the values from multiselect picklist field and add them in a map  ( map of account id and list of strings(multiselect picklist values)
// Now you have a map similiar to this
// Map(accId, [goa, kerala])
// Now define a list of contacts and traverse over the map and create a contact record and add that in list of contacts 


public static void CrateContact(List<Account> accountListNew , Map<Id, Account> newAccountMap){
    
    Map<Id, List<String>> mapOfIdToListOfStrings = new Map<Id, List<String>>();
    List<contact> newrCreatedcontacts = new List<contact>();

    for (Account acc : accountListNew) {
        if(acc.multipicklist1__c){
            List<String> multipicklistValues = acc.multipicklist1__c.split(';');
            mapOfIdToListOfStrings.put(acc.id, multipicklistValues);

            Contact newContact = new Contact(LastName = acc.name, AccountId = acc.id, Description = acc.multipicklist1__c);
            newrCreatedcontacts.push(newContact);
        }
    } 

    try {
        // Insert the contact
        insert newrCreatedcontacts;
        System.debug('Contact created successfully');
    } catch (DmlException e) {
        System.debug('Error creating contact: ' + e.getMessage());
    }

}



}