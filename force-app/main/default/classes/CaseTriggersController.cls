/**
 * @description       : 
 * @author            : Shubham Jaurat
 * @group             : 
 * @last modified on  : 01-18-2025
 * @last modified by  : Shubham Jaurat
**/
public with sharing class CaseTriggersController {
    // 1) Create tigger On case Object when case record os inserted  search Conact base on web  email address 
    // field present on case to email field on conatct an assign conatct to the case record

    public static void updateConactfield(List <case> caseList){
        List<String> emailAddresses = new List<String>();
        Map<id, string> emailMap = new Map<id, string>();

        Map<String, List<Contact>> emailToContacts = new Map<String, List<Contact>>();

        List<Contact> contactList = new List<Contact>();

        // SELECT Id, SuppliedEmail, ContactId, CaseNumber FROM Case
        if(!caseList.isEmpty()){
            for(case caseRecord : caseList){
                if(caseRecord.SuppliedEmail != null){
                    emailMap.put(caseRecord.Id, caseRecord.SuppliedEmail.toLowerCase());
                }
            }
        }

        contactList = [SELECT Id, Email FROM Contact WHERE Email IN :emailMap.values()];

        if(!contactList.isEmpty()){
            for (Contact ContactRecord : contactList) {
                String emailKey = ContactRecord.Email.toLowerCase();
                if (!emailToContacts.containsKey(emailKey)) {
                    emailToContacts.put(emailKey, new List<Contact>());
                }
                    emailToContacts.get(emailKey).add(ContactRecord);
            }
            
        }

        if(!caseList.isEmpty()){
            for (Case c : caseList) {
                if (c.SuppliedEmail != null) {
                    String emailKey = c.SuppliedEmail.toLowerCase();
                    if (emailToContacts.containsKey(emailKey)) {
                        List<Contact> ContactsListWithKey = emailToContacts.get(emailKey);
                        if(ContactsListWithKey.size() > 1){
                            c.addError('ContactId', 'there are multipal contact with same email ID', false);
                        }
                        if(ContactsListWithKey.size() == 1){
                            c.ContactId = ContactsListWithKey.get(0).Id;
                        }
                        
                    }
                }
            }
        }
        if(!caseList.isEmpty()){
            insert caseList;
        }
        
    }
}